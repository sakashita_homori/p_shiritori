class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.integer :count, default: 0
      t.boolean :is_win, default: false

      t.timestamps
    end
  end
end


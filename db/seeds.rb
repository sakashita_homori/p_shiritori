# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Pokemon.create([
    {name: 'フシギダネ'}, {name: 'フシギソウ'}, {name: 'フシギバナ'}, {name: 'ヒトカゲ'}, {name: 'リザード'}, 
    {name: 'リザードン'}, {name: 'ゼニガメ'}, {name: 'カメール'}, {name: 'カメックス'}, {name: 'キャタピー'}, 
    {name: 'トランセル'}, {name: 'バタフリー'}, {name: 'ビードル'}, {name: 'コクーン'}, {name: 'スピアー'},
    {name: 'ポッポ'}, {name: 'ピジョン'}, {name: 'ピジョット'}, {name: 'コラッタ'}, {name: 'ラッタ'},
    {name: 'オニスズメ'}, {name: 'オニドリル'}, {name: 'アーボ'}, {name: 'アーボック'}, {name: 'ピカチュウ'},
    {name: 'ライチュウ'}, {name: 'サンド'}, {name: 'サンドパン'}, {name: 'ニドラン'}, {name: 'ニドリーナ'},
    {name: 'ニドクイン'}, {name: 'ニドリーノ'}, {name: 'ニドキング'}, {name: 'ピッピ'}, {name: 'ピクシー'},
    {name: 'ロコン'}, {name: 'キュウコン'}, {name: 'プリン'}, {name: 'プクリン'}, {name: 'ズバット'},
    {name: 'ゴルバット'}, {name: 'ナゾノクサ'}, {name: 'クサイハナ'}, {name: 'ラフレシア'}, {name: 'パラス'},
    {name: 'コンパン'}, {name: 'モルフォン'}, {name: 'ディグダ'}, {name: 'ダグトリオ'}, {name: 'ニャース'},
    {name: 'ペルシアン'}, {name: 'コダック'}, {name: 'ゴルダック'}, {name: 'マンキー'}, {name: 'オコリザル'},
    {name: 'ガーディ'}, {name: 'ウィンディ'}, {name: 'ニョロモ'}, {name: 'ニョロゾ'}, {name: 'ニョロボン'},
    {name: 'ケーシィ'}, {name: 'ユンゲラー'}, {name: 'フーディン'}, {name: 'ワンリキー'}, {name: 'ゴーリキー'},
    {name: 'マダツボミ'}, {name: 'ウツドン'}, {name: 'ウツボット'}, {name: 'メノクラゲ'}, {name: 'ドククラゲ'},
    {name: 'イシツブテ'}, {name: 'ゴローン'}, {name: 'ゴローニャ'}, {name: 'ポニータ'}, {name: 'ギャロップ'},
    {name: 'ヤドン'}, {name: 'ヤドラン'}, {name: 'コイル'}, {name: 'レアコイル'}, {name: 'カモネギ'},
    {name: 'ドードー'}, {name: 'ドードリオ'}, {name: 'パウワウ'}, {name: 'ジュゴン'}, {name: 'ベトベター'},
    {name: 'ベトベトン'}, {name: 'シェルダー'}, {name: 'パルシェン'}, {name: 'ゴース'}, {name: 'ゴースト'},
    {name: 'ゲンガー'}, {name: 'イワーク'}, {name: 'スリープ'}, {name: 'スリーパー'}, {name: 'クラブ'},
    {name: 'キングラー'}, {name: 'ビリリダマ'}, {name: 'マルマイン'}, {name: 'タマタマ'}, {name: 'ナッシー'},
    {name: 'カラカラ'}, {name: 'ガラガラ'}, {name: 'サワムラー'}, {name: 'エビワラー'}, {name: 'ベロリンガ'},
    {name: 'ドガース'}, {name: 'マタドガス'}, {name: 'サイホーン'}, {name: 'サイドン'}, {name: 'ラッキー'},
    {name: 'モンジャラ'}, {name: 'ガルーラ'}, {name: 'タッツー'}, {name: 'シードラ'}, {name: 'トサキント'},
    {name: 'アズマオウ'}, {name: 'ヒトデマン'}, {name: 'スターミー'}, {name: 'バリヤード'}, {name: 'ストライク'},
    {name: 'ルージュラ'}, {name: 'エレブー'}, {name: 'ブーバー'}, {name: 'カイロス'}, {name: 'ケンタロス'},
    {name: 'コイキング'}, {name: 'ギャラドス'}, {name: 'ラプラス'}, {name: 'メタモン'}, {name: 'イーブイ'},
    {name: 'シャワーズ'}, {name: 'サンダース'}, {name: 'ブースター'}, {name: 'ポリゴン'}, {name: 'オムナイト'},
    {name: 'オムスター'}, {name: 'カブト'}, {name: 'カブトプス'}, {name: 'プテラ'}, {name: 'カビゴン'},
    {name: 'フリーザー'}, {name: 'サンダー'}, {name: 'ミニリュウ'}, {name: 'ハクリュー'}, {name: 'カイリュー'},
    {name: 'ミュウツー'}, {name: 'ミュウ'}, {name: 'パラセクト'}, {name: 'カイリキー'}, {name: 'ファイヤー'}
    ])